package com.zerobeta.authserver.controller;

import com.zerobeta.authserver.model.ConfirmData;
import com.zerobeta.authserver.model.SignUpData;
import com.zerobeta.authserver.model.UserData;
import com.zerobeta.authserver.model.VerifyRequest;
import com.zerobeta.authserver.services.EMailService;
import com.zerobeta.authserver.services.IdentityProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
public class AuthController {

    @Autowired
    private IdentityProviderClient identityProviderClient;
    @Autowired
    private EMailService emailService; 

    @RequestMapping(value = "signup", method= RequestMethod.POST)
    public ResponseEntity<Object>  signUp(@RequestBody SignUpData signUpData) {
        identityProviderClient.signUpUser(signUpData);
        return new ResponseEntity<>(signUpData, HttpStatus.OK);
    }

    @RequestMapping(value = "confirm", method= RequestMethod.POST)
    public ResponseEntity<Object>  confirm(@RequestBody ConfirmData confirmData) {
           UserData data = identityProviderClient.confirmUser(confirmData);
           emailService.generateEMail(data.getEmail(), data.getPassword(), data.getRegistration_number());
           return new ResponseEntity<>(confirmData, HttpStatus.OK);
    }

    //Not secure but will be used only by internal services
    @RequestMapping(value = "verify", method= RequestMethod.GET)
    public ResponseEntity<Object>  verify(@RequestParam String auth_type, @RequestParam String auth_details) {
        VerifyRequest verifyRequest = new VerifyRequest();
        verifyRequest.setAuth_details(auth_details);
        verifyRequest.setAuth_type(auth_type);
        boolean isSuccess =  identityProviderClient.verifyUser(verifyRequest);
        if(isSuccess)
        {
            return new ResponseEntity<>("verified", HttpStatus.OK);
        }
        else{

            return new ResponseEntity<>("AuthFailure", HttpStatus.UNAUTHORIZED);
        }
    }
}

