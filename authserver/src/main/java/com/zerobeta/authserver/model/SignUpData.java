package com.zerobeta.authserver.model;

public class SignUpData {
    private String registration_number;
    private String company_name;
    private String company_address;
    private String email;
    private String contact_number;

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getCompany_address() {
        return company_address;
    }

    public String getEmail() {
        return email;
    }

    public String getContact_number() {
        return contact_number;
    }

    @Override
    public String toString() {
        return "SignUpData{" +
                "registration_number='" + registration_number + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_address='" + company_address + '\'' +
                ", email='" + email + '\'' +
                ", contact_number='" + contact_number + '\'' +
                '}';
    }
}
