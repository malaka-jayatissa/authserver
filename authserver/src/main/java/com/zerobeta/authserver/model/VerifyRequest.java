package com.zerobeta.authserver.model;

public class VerifyRequest {
    String auth_type;
    String auth_details;
    
    public String getAuth_type() {
        return auth_type;
    }
    public void setAuth_type(String auth_type) {
        this.auth_type = auth_type;
    }
    public String getAuth_details() {
        return auth_details;
    }
    public void setAuth_details(String auth_details) {
        this.auth_details = auth_details;
    }
    
}
