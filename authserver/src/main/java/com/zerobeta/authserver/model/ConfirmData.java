package com.zerobeta.authserver.model;

public class ConfirmData {
    private String registration_number;

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

   
}
