package com.zerobeta.authserver.services;

import com.zerobeta.authserver.model.ConfirmData;
import com.zerobeta.authserver.model.SignUpData;
import com.zerobeta.authserver.model.UserData;
import com.zerobeta.authserver.model.VerifyRequest;


public interface IdentityProviderClient {

    public boolean signUpUser(SignUpData signUpData);

    public UserData confirmUser(ConfirmData confirmData);

    public boolean verifyUser(VerifyRequest request);
}
