package com.zerobeta.authserver.services.aws;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.zerobeta.authserver.services.EMailService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class AWSSESMailService extends EMailService {

    @Value("${email.sender.address}")
    private String senderEmail;
    private static final Logger logger = LoggerFactory.getLogger(AWSSESMailService.class);

    @Override
    protected void sendMail(String emailID, String body, String emailHeader) {
            logger.info(emailID + " " + body + " " + emailHeader + " " + senderEmail);
        AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.defaultClient();
        SendEmailRequest request = new SendEmailRequest()
                .withDestination(new Destination()
                        .withToAddresses(emailID))
                .withMessage(new Message()
                        .withSubject(new Content()
                                .withCharset("UTF-8")
                                .withData(emailHeader))
                        .withBody(new Body().withText(new Content().withCharset("UTF-8").withData(body))))
                .withSource(senderEmail);

        client.sendEmail(request);
    }

}