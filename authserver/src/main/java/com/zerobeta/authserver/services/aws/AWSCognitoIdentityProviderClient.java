package com.zerobeta.authserver.services.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.zerobeta.authserver.model.ConfirmData;
import com.zerobeta.authserver.model.SignUpData;
import com.zerobeta.authserver.model.UserData;
import com.zerobeta.authserver.model.VerifyRequest;
import com.zerobeta.authserver.services.IdentityProviderClient;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminConfirmSignUpRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminGetUserRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminGetUserResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminInitiateAuthRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminInitiateAuthResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminInitiateAuthResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminGetUserResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminSetUserPasswordRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AuthFlowType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.SignUpRequest;

@Service
public class AWSCognitoIdentityProviderClient implements IdentityProviderClient {

    private static final Logger logger = LoggerFactory.getLogger(AWSCognitoIdentityProviderClient.class);
    @Value("${idp.clientid}")
    private String clientID;
    @Value("${idp.password.length}")
    private int passwordLength;
    @Value("${idp.poolid}")
    private String poolID;


    @Override
    public boolean signUpUser(SignUpData signUpData) {
        logger.info("Sign up user = " + signUpData);
        CognitoIdentityProviderClient client = CognitoIdentityProviderClient.create();
        ArrayList<AttributeType> attributes = new ArrayList<>();
        attributes.add(AttributeType.builder().name("email").value(signUpData.getEmail()).build());
        attributes.add(AttributeType.builder().name("phone_number").value(signUpData.getContact_number()).build());
        attributes.add(AttributeType.builder().name("address").value(signUpData.getCompany_address()).build());
        attributes.add(AttributeType.builder().name("name").value(signUpData.getCompany_name()).build());

        SignUpRequest request = SignUpRequest.builder().clientId(clientID)
                .password(RandomStringUtils.random(passwordLength, true, true))
                .userAttributes(attributes)
                .username(signUpData.getRegistration_number()).build();

        client.signUp(request);
        return true;
    }

    @Override
    public UserData confirmUser(ConfirmData confirmData) {
        CognitoIdentityProviderClient client = CognitoIdentityProviderClient.create();
        AdminConfirmSignUpRequest request = AdminConfirmSignUpRequest.builder()
                .username(confirmData.getRegistration_number())
                .userPoolId(poolID)
                .build();
        client.adminConfirmSignUp(request);
        String newPassword = RandomStringUtils.random(passwordLength, true, true);
        AdminSetUserPasswordRequest userPassWordRequest = AdminSetUserPasswordRequest.builder()
                .password(newPassword)
                .username(confirmData.getRegistration_number())
                .userPoolId(poolID)
                .permanent(true)
                .build();
        client.adminSetUserPassword(userPassWordRequest);
        AdminGetUserRequest userRequest = AdminGetUserRequest.builder()
                .username(confirmData.getRegistration_number())
                .userPoolId(poolID)
                .build();
        AdminGetUserResponse getUserResponse = client.adminGetUser(userRequest);
        logger.info("Retreiving user info");
        logger.info(getUserResponse.toString());
        List<AttributeType> attributes =  getUserResponse.userAttributes();
        String emailAddress = "";
        for(AttributeType attribute: attributes)
        {
                if(attribute.name().equals("email"))
                {
                        logger.info("Email found");
                        emailAddress = attribute.value();
                        logger.info("Email found" + emailAddress);//Todo remove log
                }
        }

        UserData userData = new UserData();
        userData.setEmail(emailAddress);
        userData.setPassword(newPassword);
        userData.setRegistration_number(confirmData.getRegistration_number());
        return userData;
    }

    @Override
    public boolean verifyUser(VerifyRequest request)
    {
        if(request.getAuth_type().equals("BASIC") == false)
        {
                //Todo Throw exception
                logger.error("Only support basic authentication");
                //Only Basic Authentication is available yet
                return false;
        }

        String[] arrOfStr = request.getAuth_details().split(":");
        if(arrOfStr.length != 2)
        {
                //Todo throw error
                logger.error("Basic Authentication invalid format");
                return false;
        }
        Map<String,String> authParameters = new HashMap<>();
        authParameters.put("USERNAME", arrOfStr[0]);
        authParameters.put("PASSWORD",arrOfStr[1]);
        CognitoIdentityProviderClient client = CognitoIdentityProviderClient.create();
        AdminInitiateAuthRequest authRequest = AdminInitiateAuthRequest.builder()
                                           .authFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                                           .authParameters(authParameters)
                                           .clientId(clientID)
                                           .userPoolId(poolID)
                                           .build();
        
        //Exception handling is not done in this scope
        try
        {
                AdminInitiateAuthResponse response  = client.adminInitiateAuth(authRequest);
                logger.info(response.toString());
                return true;
        }
        catch (Exception e)
        {
                logger.error(e.getMessage());
                return false;
        }
            
       
    }
}
