package com.zerobeta.authserver.services;

public abstract class EMailService {

    public void generateEMail(String emailID, String password, String userID) {
        String email = "Onetime password for userID " + userID + "Is " + password;
        String header = "ZeroBeta User SignUp for user " + userID;
        sendMail(emailID, email,header);
    }

    protected abstract void sendMail(String emailID, String body, String emailHeader);
}
